{-# LANGUAGE NamedFieldPuns #-}

module Decision
  ( decisionEngines,
    human,
    randomAI,
  )
where

import Board (putBoard)
import Data.Char (intToDigit)
import GameTree (GameTree (..))
import Move (Move, prettyMove)
import State (KnowledgeBase (board), State (State, kb, moves))
import System.Random (Random (randomR), newStdGen)
import Text.Read (readEither)

decisionEngines :: [([Char], State -> IO Move)]
decisionEngines = [("human", human), ("random", randomAI)]

human :: State -> IO Move
human s@State {kb, moves} = do
  putStrLn "Make a move by choosing a number from the list or by giving FROM and TO positions."
  putStrLn "Valid inputs: e2e4 / e2 e4 / 0-0 / 0-0-0 / g8Q."
  let aMoves = zip [1 ..] moves
  putStrLn $ foldl (\acc (i, m) -> acc ++ show i ++ ". " ++ prettyMove (board kb) m ++ " | ") "" aMoves
  putBoard $ board kb
  putStr "input: "
  input <- getLine
  case readEither input :: Either String Int of
    Left _ -> case readEither input :: Either String Move of
      Left _ -> putStrLn "Invalid move." >> human s
      Right move -> if move `elem` moves then return move else putStrLn "Illegal move." >> human s
    Right i -> case lookup i aMoves of
      Just move -> return move
      Nothing -> putStrLn ("No " ++ show i ++ "th index.") >> human s

randomAI :: State -> IO Move
randomAI State {kb, moves} = do
  putStrLn "RandomAI's turn..."
  gen <- newStdGen
  putBoard $ board kb
  let (randomI, _) = randomR (0, length moves - 1) gen
  return $ moves !! randomI

minmaxAI :: State -> IO Move
minmaxAI s@(State kb moves controlState) = do
  putStrLn "minmaxAI's turn..."
  return $ best $ Node s []
  where
    best :: GameTree State -> Move
    best tree = undefined
