---
title: Feladatleírás
papersize: a4
geometry: margin=1.2in
fontsize: 12pt
---

A szakdolgozat témája, egy sakkprogram elkészítése tisztán funkcionális programozást felhasználva. A játszó felek lehessenek emberek és robotok egyaránt (alap mesterséges intelligencia implementálásával), a játszmák nyomon követhetőek arra alkalmas grafikus felületen keresztül, például terminálban ASCII karakterekkel, vagy a böngészőben.

A programot Haskell-ben írd[^1], a fejlesztés során szintaxisát, nyelvi elemeit lépcsőzetesen vezesd be. Ismertesd a funkcionális programozási paradigma tulajdonságait, különbségeit a napjainkban jobban megszokott imperatív programozással szemben.

Kódrészleteken keresztül mutasd be a megoldáshoz vezető út akadályait és azok leküzdéséhez használt gondolatmeneted, ahol indokolt, ott használj egyszerű matematikai jelöléseket a problémák leírására és megoldások helyességenek bizonyítására.

[^1]: Főként Haskell-ben, de szituáció függően akár Elm-ben, ha webes front-end-ed szeretnél.
