module Color
  ( Color (White, Black),
    next,
    colorMultiplier,
    Castleable (..),
  )
where

data Color = White | Black deriving (Show, Eq, Ord)

next :: Color -> Color
next White = Black
next Black = White

colorMultiplier :: Color -> Int
colorMultiplier White = 1
colorMultiplier Black = -1

data Castleable = Castleable
  { kingSide :: Bool,
    queenSide :: Bool
  }
  deriving (Show)
