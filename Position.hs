module Position
  ( Pos (Pos, rank, file),
    Displacement (Displacement, drank, dfile),
    fileToHumanReadable,
    normalise,
    unifyRank,
    fromIndex,
    toIndex,
    fromString,
    applyDisplacement,
    validPos,
  )
where

import Color (Color (Black))
import Data.Char (chr, digitToInt, ord)

-- | (rank, file)
data Pos = Pos
  { rank :: Int,
    file :: Int
  }
  deriving (Eq)

-- | Converts position into human readable.
instance Show Pos where
  show (Pos r f) = fileToHumanReadable f : show (r + 1)

instance Read Pos where
  readsPrec _ (f : r : tail) =
    if validPos p
      then (p, tail) : reads tail
      else []
    where
      fInt = ord f - ord 'a'
      rInt = ord r - ord '1'
      p = Pos rInt fInt
  readsPrec _ _ = []

data Displacement = Displacement
  { drank :: Int,
    dfile :: Int
  }
  deriving (Eq)

fileToHumanReadable :: Int -> Char
fileToHumanReadable f = chr $ f + 97

normalise :: Displacement -> Displacement
normalise d = Displacement (if drank d == 0 then 0 else signum $ drank d) (if dfile d == 0 then 0 else signum $ dfile d)

unifyRank :: Int -> Color -> Int
unifyRank r c = if c == Black then 7 - r else r

-- >>> toIndex (Pos 7 7)
-- 63
toIndex :: Pos -> Int
toIndex (Pos r f) = r * 8 + f

-- >>> fromIndex 63
-- h8
fromIndex :: Int -> Pos
fromIndex i = uncurry Pos j
  where
    j = i `quotRem` 8

-- | Translates human readable position to `Pos`.
--
-- Assumes safe input.
-- >>> fromString "a2"
-- a2
fromString :: String -> Pos
fromString s =
  let f = head s
      r = head $ tail s
   in Pos (digitToInt r - 1) (ord f - 97)

applyDisplacement :: Pos -> Displacement -> Pos
applyDisplacement p d = Pos (rank p + drank d) (file p + dfile d)

-- | Position on the board.
validPos :: Pos -> Bool
validPos p = rank p >= 0 && rank p < 8 && file p >= 0 && file p < 8
