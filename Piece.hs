module Piece
  ( PieceColor,
    PieceType (..),
    Piece (..),
    prettyPiece,
    valuePiece,
    lineAttacker,
    diagonalAttacker,
  )
where

import Color (Color (Black, White), colorMultiplier)

type PieceColor = Color

data PieceType = Pawn | Knight | Bishop | Rook | Queen | King deriving (Eq)

instance Show PieceType where
  show Pawn = "P"
  show Knight = "N"
  show Bishop = "B"
  show Rook = "R"
  show Queen = "Q"
  show King = "K"

instance Read PieceType where
  readsPrec _ (piece : tail)
    | piece == 'P' = (Pawn, tail) : reads tail
    | piece == 'N' = (Pawn, tail) : reads tail
    | piece == 'B' = (Pawn, tail) : reads tail
    | piece == 'R' = (Pawn, tail) : reads tail
    | piece == 'Q' = (Pawn, tail) : reads tail
    | piece == 'K' = (Pawn, tail) : reads tail
    | otherwise = []
  readsPrec _ _ = []

data Piece = Piece
  { color :: PieceColor,
    name :: PieceType
  }
  deriving (Eq)

instance Show Piece where
  show (Piece c t) = show c ++ show t

prettyPiece' :: Piece -> String
prettyPiece' (Piece c t) = show c ++ show t

prettyPiece :: Piece -> String
prettyPiece (Piece Black Pawn) = "♙"
prettyPiece (Piece Black Knight) = "♘"
prettyPiece (Piece Black Bishop) = "♗"
prettyPiece (Piece Black Rook) = "♖"
prettyPiece (Piece Black Queen) = "♕"
prettyPiece (Piece Black King) = "♔"
prettyPiece (Piece White Pawn) = "♟"
prettyPiece (Piece White Knight) = "♞"
prettyPiece (Piece White Bishop) = "♝"
prettyPiece (Piece White Rook) = "♜"
prettyPiece (Piece White Queen) = "♛"
prettyPiece (Piece White King) = "♚"

valuePiece :: Piece -> Int
valuePiece (Piece c t) =
  colorMultiplier c
    * case t of
      Pawn -> 1
      Knight -> 3
      Bishop -> 3
      Rook -> 5
      Queen -> 9
      King -> 1000

-- | Returns wheter a `PieceType` can attack in a line continuously.
lineAttacker :: PieceType -> Bool
lineAttacker t = t == Rook || t == Queen

-- | Returns wheter a `PieceType` can attack diagonally continuously.
diagonalAttacker :: PieceType -> Bool
diagonalAttacker t = t == Bishop || t == Queen
