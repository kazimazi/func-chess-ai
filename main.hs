{-# LANGUAGE BangPatterns #-}

import Board (putBoard)
import Control.Exception (catch)
import Data.Maybe (fromMaybe, isJust, isNothing)
import Decision (decisionEngines, human, randomAI)
import GameTree
import Move (Move)
import State
  ( ControlState (Draw, InPlay, Won),
    KnowledgeBase (board),
    State (controlState, kb),
    doMove,
    initialState,
  )
import System.Environment (getArgs)

gameLoop :: (State -> IO Move) -> (State -> IO Move) -> State -> IO ()
gameLoop a b s =
  case controlState s of
    InPlay -> do
      move <- a s
      putStrLn ("Move: " ++ show move) >> putStrLn "########"
      gameLoop b a $ doMove s move
    Won c -> putBoard (board (kb s)) >> putStrLn (show c ++ " won!")
    Draw -> putBoard (board (kb s)) >> putStrLn "Draw!"

main :: IO ()
main = do
  args <- getArgs
  (!engine1, !engine2) <- parseArgs args
  gameLoop engine1 engine2 initialState
  where
    parseArgs :: [String] -> IO (State -> IO Move, State -> IO Move)
    parseArgs [] = putStrLn "WARNING! No arguments, falling back to default: human, random." >> return (human, randomAI)
    parseArgs (arg1 : arg2 : _) =
      let (maybeEngine1, maybeEngine2) = (lookup arg1 decisionEngines, lookup arg2 decisionEngines)
          parsedEngines =
            ( fromMaybe (error ("ERROR! Can't recognize \"" ++ arg1 ++ "\" as valid engine.")) maybeEngine1,
              fromMaybe (error ("ERROR! Can't recognize \"" ++ arg2 ++ "\" as valid engine.")) maybeEngine2
            )
       in return parsedEngines
    parseArgs _ = error "ERROR! Can't recognize arguments as valid engines."
