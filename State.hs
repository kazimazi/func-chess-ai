{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE NamedFieldPuns #-}

module State
  ( ControlState (..),
    KnowledgeBase (..),
    State (..),
    initialState,
    doMove,
    nextStates,
  )
where

import Board (Board, colorPos, initialBoard)
import Color (Castleable (..), Color (..), next)
import Data.Foldable (foldl')
import qualified Data.Map as M
import Data.Maybe (fromMaybe)
import Move
  ( Attack (Direct, Indirect),
    Exploration (Exploration),
    Move (KingSideCastle, Move, QueenSideCastle),
    applyMove,
    attackWalks,
    genMoves, isAttacked
  )
import Position (Pos (Pos), unifyRank)

data ControlState = InPlay | Won Color | Draw deriving (Show)

type KingState = M.Map Color Pos

type CastleState = M.Map Color Castleable

initialCastleState :: CastleState
initialCastleState = M.fromList [(White, castleable), (Black, castleable)]
  where
    castleable = Castleable True True

-- | All we have to know about the board, to figure out legal moves.
data KnowledgeBase = KnowledgeBase
  { board :: Board,
    lastMove :: Maybe Move,
    kingState :: KingState,
    castleState :: CastleState,
    color :: Color
  }
  deriving (Show)

initialKB :: KnowledgeBase
initialKB =
  KnowledgeBase
    { board = initialBoard,
      lastMove = Nothing,
      kingState = M.fromList [(White, Pos 0 4), (Black, Pos 7 4)],
      castleState = initialCastleState,
      color = White
    }

data State = State
  { kb :: KnowledgeBase,
    -- | Legal moves at current `State`.
    -- This value is essential to know when we have to stop the game.
    moves :: [Move],
    -- | Depending on `moves` we can set `ControlState`.
    controlState :: ControlState
  }
  deriving (Show)

initialState :: State
initialState =
  State
    { kb = initialKB,
      moves = allMoves initialKB,
      controlState = InPlay
    }

allMoves :: KnowledgeBase -> [Move]
allMoves KnowledgeBase {board = b, lastMove = lastMove, kingState = kingState, castleState = castleState, color = c} =
  concatMap
    ( \from ->
        let checkInfo@(checkWalks, discoveredCheckWalk, isCheck, isDiscoveredCheckDanger, isKingMustMove) =
              foldl'
                ( \(checkWalks, discoveredCheckWalk, isCheck, isDiscoveredCheckDanger, isKingMustMove) attack ->
                    case attack of
                      Direct (Exploration walk _ _) ->
                        (walk : checkWalks, discoveredCheckWalk, True, isDiscoveredCheckDanger, isCheck)
                      Indirect (Exploration walk _ _) p' ->
                        (checkWalks, if from == p' then walk else discoveredCheckWalk, isCheck, from == p', isKingMustMove)
                )
                ([], [], False, False, False)
                $ attackWalks b c kingPos True
         in genMoves b from lastMove castleable checkInfo
    )
    $ colorPos c b
  where
    kingPos :: Pos
    kingPos = kingState M.! c
    castleable :: Castleable
    castleable = castleState M.! c

resolveKingState :: KingState -> Move -> Color -> KingState
resolveKingState kingState m c = case m of
  KingSideCastle -> M.update (\_ -> Just (Pos (unifyRank 0 c) 6)) c kingState
  QueenSideCastle -> M.update (\_ -> Just (Pos (unifyRank 0 c) 2)) c kingState
  Move from to -> M.update (\p -> if p == from then Just to else Just p) c kingState
  _ -> kingState

resolveCastleState :: CastleState -> Move -> Color -> CastleState
resolveCastleState castleState m c = case m of
  KingSideCastle -> let justCastleable = Just $ Castleable False False in M.update (const justCastleable) c castleState
  QueenSideCastle -> let justCastleable = Just $ Castleable False False in M.update (const justCastleable) c castleState
  Move from to -> resolvePos to $ resolvePos from resolveKingPos
    where
      resolveKingPos =
        if
            | from == Pos 0 4 -> M.update (const justNotCastleable) White castleState
            | from == Pos 7 4 -> M.update (const justNotCastleable) Black castleState
            | otherwise -> castleState
        where
          justNotCastleable = Just $ Castleable False False
      resolvePos p castleState' =
        if
            | p == Pos 0 7 -> M.update (\castleable -> Just castleable {kingSide = False}) White castleState
            | p == Pos 7 7 -> M.update (\castleable -> Just castleable {kingSide = False}) Black castleState
            | p == Pos 0 1 -> M.update (\castleable -> Just castleable {queenSide = False}) White castleState
            | p == Pos 7 1 -> M.update (\castleable -> Just castleable {queenSide = False}) Black castleState
            | otherwise -> castleState
  _ -> castleState

-- | Given the current `State` and a `Move`, jumps to the next `State`.
doMove :: State -> Move -> State
doMove State {kb} m =
  State
    { kb = kb',
      moves = moves,
      controlState = controlState
    }
  where
    kb' =
      KnowledgeBase
        { board = applyMove (board kb) m (color kb),
          lastMove = Just m,
          kingState = resolveKingState (kingState kb) m (color kb),
          castleState = resolveCastleState (castleState kb) m (color kb),
          color = next . color $ kb
        }
    moves = allMoves kb'
    controlState =
      if null moves
        then if isAttacked (board kb') (color kb') (kingState kb' M.! color kb') then Won (color kb) else Draw
        else InPlay

nextStates :: State -> [State]
nextStates s = map (doMove s) $ moves s
