module GameTree
  ( GameTree (..),
    genGameTree,
    prune,
  )
where

import State (State (..), nextStates)

data GameTree a = Node a [GameTree a] deriving (Show)

-- WIP: This is a good starting point.

genGameTree :: State -> GameTree State
genGameTree s = Node s $ map genGameTree (nextStates s)

prune :: Int -> GameTree a -> GameTree a
prune 0 (Node a _) = Node a []
prune n (Node a children) = Node a $ map (prune $ n - 1) children
