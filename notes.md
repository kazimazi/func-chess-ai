# Infos

These are notes and not documentation!

## Sources
First I tried following this haskell wiki [article](https://wiki.haskell.org/Learning_Haskell_with_Chess) alapjan.

Information on starting the game + insight to practical IOs on [link](https://stackoverflow.com/questions/33769015/repeatedly-applying-function-to-game-board-in-haskell).

## Development

For writing this program I made use of [HLS](https://github.com/haskell/haskell-language-server/blob/master/plugins/hls-eval-plugin/README.md) and all it's advanteges.

### How to debug

The first tool programmers are thought is `print`... so for me it has to be the almost haskell equivalent, `trace`. Or `htrace` which does the same, but indents it.
```haskell
htrace :: [Char] -> a -> a
```

### How to set command line arguments inside ghci

```
:set args arg1 arg2
```

## Noteworthy decisions

### Assumptions

I'm making AI for the REAL chess game, so a board is going to consist of 64 squares.

### TupleSections

**REMOVED IT since I changed piece from a type synomn into a data type**

It's an extension that allows some logical moves.

```haskell
{-# LANGUAGE TupleSections #-}
-- with TupleSections
let f = (, True)
f False
-- (False,True)

-- without them
let f = \x -> (x, True)
f False
-- (False,True)
```

To enable it in ghci, use `:set -XTupleSections` command.

### Representation of ...

#### Piece

Why did I even make it as a type synonym? It's beyond me.\
Anyway it should make sence for it to be a data type.

#### Square

Makes sence to introduce record syntax with it, since I often need to access value in it.

#### Board

Thought about using `[Row]` (list of Rows of Squares) but decided not to, for simplicity's sake.

First, I tried to use `[Squares]` (list of Squares), but on a chess board I need to reference squeres by coodrinates quiet often and that leads to cycling through it many-many times which could be avoidable, otherwise.\
[This post](https://softwareengineering.stackexchange.com/questions/322365/data-structure-for-two-dimensional-board-games-in-functional-languages) on the net recommends keeping the board in a `Map`.

Maybe other languages would have stricter types, like Idris.

There are lot of options in haskell, not sure which are the best suited for this tho.
- Vector
- Map
- Array
- Sequence
- good'ol List

I'm going to go with `List` and see how long it takes.

If list is not ordered by positions, then `prettySquare` function will not work properly.

##### Printing multiple boards to std output

`mapM` does the job.

### Construction of the board

Into a list is pretty straight forward.

### Pretty printing

When I want to represent the board in a human consumable form I use various unicode characters to get a nice appeal, see it on [wikipedia](https://en.wikipedia.org/wiki/Box-drawing_character).

My pretty printing functions return with `String`, and they look as such: `"\9474BN\9474\n"`.\
Not that nice, that's why they then need to be put out to the `IO`, with a `putStr`.

### Issues with some of the patterns

At a certain point I got patterns like.
```haskell
colorPos _ [] = ...
colorPos c ((_, Nothing) : b) = ...
colorPos c1 ((pos, Just (c2, _)) : b) = ...
```
That's not so nice.

**I could resolve it with the usage of data types, record syntax and function `maybe`.**

### Testing performance

I should be able to do it with relative ease and while I'm at it, might as well spit the main file up into modules.

The most basic way of profiling is in ghci by enabling with some stats with `:set s`. Not professional!

### Moving a piece

To move a single piece on the board I currently iterate over the squares 3 times.

```haskell
movePos :: Pos -> Pos -> Board -> Board
movePos p1 p2 b = deleteSquare p1 $ updateBoard p2 piece b
  where
    piece = pieceAt p1 b
```

1. find the piece and store it
2. remove piece from p2
3. find position it should go to

No need to say, this is not ideal.

Solution would be...
1. iterating over list
2. if we reach point of interest we "lift" the piece or insert something we might find later on.
3. then do the other act

..p1.......p2....
..p2.......p1....

My head hurts. Might as well just drop the whole `List` thing in favor of `Vectors`.

But I didn't give up and started googling and found [this](https://stackoverflow.com/questions/30551033/swap-two-elements-in-a-list-by-its-indices/38300185#38300185) on stackoverflow.

<!-- TODO write how it works -->

### Generation moves for a given piece

Should remember the last move, if any, so that calculations about check and en passant become possible.

### Modules

Drew inspiration from [this chess project](https://github.com/OlivierNicole/haskell-chess).

Could separate them into:

- `Position`
- `Board`
- `Color`
- `Piece`
- `Rules`
- `Move`
- `State`
- `Decision`

### How to check for check?

#### Primitive way

Just find `King` and check if attacked.

#### Bit better

When generating moves for a given piece, I could provide the function with the previous move, making it easier to determine, if we are checked.

#### Best (as of now)

Before generating moves (`genMoves`), check if I'm checked and __extract the postion vectors of checks__.\
The position vector can be empty, can have 1 elem, or 2 elems, maybe more if the board is impossible.

If it's more then 1, then only King is allowed to move.\
If it's 1, then if not King moves, then we must block the check.\
If it's 0 then anyone can move.\
Taking all this into account we must not selfcheck our-selves.

### What I did to `genMoves`?

**That function felt pretty clunky.**

It used to return `[Board]`, doing a bit too much. During pruning of the game tree some of the boards might not even be used at all.

It should return `[Move]` type, and a separate function could apply those moves to the board.

### Ending a game

Flow of the game:\
player A's turn > check if A won/draw > playerB's turn > check if B won/draw > repeat

# TODOs

- [ ] For graphical interface I should check Elm, or find an altrenative (Gloss).
- [X] Introduce moduels.
- [ ] Have satisfying coverage of [haddocs](https://kowainik.github.io/posts/haddock-tips).
- [X] Some of my patters look horrendous.
- [X] Check for discovered check.
- [X] Fix bug in `attacked`.
- [X] Fix pawn promotion. (didn't work when the pawn promots with a capture)
- [X] Fix check check. Doesn't work for Queen for example. >:(
- [X] How do I stop??? Don't know enough haskell to figure it out!
- [ ] Make a log thingy to follow moves, like in chess books.
- [ ] Have order in `notes.md` and decide what I need as TODO and what I don't.
