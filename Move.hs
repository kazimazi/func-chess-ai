{-# LANGUAGE LambdaCase #-}

module Move
  ( Move (..),
    Exploration (..),
    Attack (..),
    prettyMove,
    explore,
    attackWalks,
    applyMove,
    genMoves,
    isAttacked,
  )
where

import Board (Board, Square (Square, piece, position), deleteSquare, movePos, pieceAt, updateBoard)
import Color (Castleable (..), Color, colorMultiplier, next)
import Data.Either (isRight)
import Data.Maybe (catMaybes, isJust, isNothing)
import Piece (Piece (..), PieceType (..), diagonalAttacker, lineAttacker)
import Position (Displacement (..), Pos (..), applyDisplacement, fileToHumanReadable, normalise, unifyRank, validPos)
import Text.Read (readEither)

-- | A `Move` describe an action on the `Board` given we know which player performs it.
data Move
  = Move Pos Pos
  | KingSideCastle
  | QueenSideCastle
  | PawnPromotion Pos Pos PieceType
  deriving (Eq)

instance Show Move where
  show (Move p1 p2) = show p1 ++ " " ++ show p2
  show KingSideCastle = "0-0"
  show QueenSideCastle = "0-0-0"
  show (PawnPromotion p1 p2 t) = show p1 ++ " " ++ show p2 ++ " " ++ show t

instance Read Move where
  readsPrec _ ('0' : '-' : '0' : '-' : '0' : tail) = (QueenSideCastle, tail) : reads tail
  readsPrec _ ('0' : '-' : '0' : tail) = (KingSideCastle, tail) : reads tail
  readsPrec _ (r : f : ' ' : r' : f' : tail) = reads (r : f : r' : f' : tail)
  readsPrec _ (r : f : r' : f' : tail) =
    if isRight eitherFrom && isRight eitherTo -- if both positions are parsable
      then (move, tail) : reads tail
      else []
    where
      eitherFrom = readEither (r : [f]) :: Either String Pos
      eitherTo = readEither (r' : [f']) :: Either String Pos
      move = Move (read (r : [f]) :: Pos) (read (r' : [f']) :: Pos)
  readsPrec _ _ = []

-- | Shows what a move is.
--
-- Examples: e4, 0-0, 0-0-0, Nf3, Nbd5, exd5, Nd1xe3
prettyMove :: Board -> Move -> String
prettyMove b m@(PawnPromotion p1 p2 t) = show p2 ++ show t
prettyMove b (Move p1 p2) =
  maybe
    (error "no piece")
    ( \piece ->
        let allAttackers =
              concatMap
                ( \case
                    Direct (Exploration walk _ _) -> walk
                    _ -> error "Doesn't make sense to see anything but Check here!"
                )
                $ attackWalks b (next (color piece)) p2 False
            isRankContenders = not $ null [p | p <- allAttackers, rank p == rank p1, file p /= file p1, pieceAt b p == maybePiece, name piece /= Pawn]
            isFileContenders = not $ null [p | p <- allAttackers, file p == file p1, rank p /= rank p1, pieceAt b p == maybePiece]
         in (if name piece /= Pawn then show (name piece) else if file p1 /= file p2 then [fileToHumanReadable (file p1)] else "")
              ++ (if isRankContenders then [fileToHumanReadable (file p1)] else "")
              ++ (if isFileContenders then show (rank p1) else "")
              ++ (if isCapture || (name piece == Pawn && file p1 /= file p2) then "x" else "")
              ++ show p2
    )
    maybePiece
  where
    maybePiece = pieceAt b p1
    isCapture = isJust $ pieceAt b p2
prettyMove _ m = show m -- like castles

-- | For every `PieceType` except for `Pawn` lists which directions they can move to.
moveDirections :: PieceType -> [Displacement]
moveDirections Knight = [Displacement 2 1, Displacement 1 2, Displacement (-1) 2, Displacement (-2) 1, Displacement (-2) (-1), Displacement (-1) (-2), Displacement 1 (-2), Displacement 2 (-1)]
moveDirections Bishop = [Displacement 1 1, Displacement (-1) 1, Displacement (-1) (-1), Displacement 1 (-1)]
moveDirections Rook = [Displacement 0 1, Displacement (-1) 0, Displacement 0 (-1), Displacement 1 0]
moveDirections Queen = [Displacement 1 1, Displacement 0 1, Displacement (-1) 1, Displacement (-1) 0, Displacement (-1) (-1), Displacement 0 (-1), Displacement 1 (-1), Displacement 1 0]
moveDirections King = [Displacement 1 1, Displacement 0 1, Displacement (-1) 1, Displacement (-1) 0, Displacement (-1) (-1), Displacement 0 (-1), Displacement 1 (-1), Displacement 1 0]
moveDirections _ = []

-- | Walk is consecutive `Pos` on the `Board`.
-- It is different for every `Piece`.
-- For `Pawn`, `Knight` and `King` that's a singleton list.
type Walk = [Pos]

data Obstacle
  = -- | We have no information on what stopped the exploration.
    Nil
  | -- | Bound of the board.
    NoSpace
  | -- | Friendly piece is blocking the way.
    Friendly
  | -- | Enemy piece is blocking the way.
    Enemy
  deriving (Eq, Show)

-- | Distance of how far a `Piece` is allowed to move, it's either `One` of `Inf`.
data Distance = One | Inf deriving (Eq)

-- | Exploration is a `Walk`, it's last `Square` and the `Obstacle` + `Pos` of what ended the Exploration.
data Exploration = Exploration Walk (Maybe Square) (Obstacle, Pos) deriving (Show)

-- | Returns all the accessable positions in all the directions.
explore ::
  -- | Given a board.
  Board ->
  -- | Exploration starts from this position.
  Pos ->
  -- | Different pieces can explore different `Pos`itions.
  Piece ->
  -- | How far we explore.
  Distance ->
  -- | Return an `Exploration` for every direction.
  [Exploration]
explore b p piece distance =
  map (exploreDirection b p piece distance)
    . moveDirections
    $ name piece

exploreDirection ::
  -- | Given a board.
  Board ->
  -- | Exploration starts from this position.
  Pos ->
  -- | Different pieces can explore different `Pos`itions.
  Piece ->
  -- | How far we explore.
  Distance ->
  -- | Direction.
  Displacement ->
  -- | Return a singular `Exploration`.
  Exploration
exploreDirection b p piece distance d = exploreDirectionHelper p Nothing
  where
    exploreDirectionHelper :: Pos -> Maybe Square -> Exploration
    exploreDirectionHelper p maybeLastVisitedSquare =
      if validPos to
        then
          maybe
            ( let (Exploration walk maybeSquare obstacle) = exploreDirectionHelper to (Just $ Square to Nothing)
               in if distance == One
                    then Exploration [to] (Just $ Square to Nothing) (Nil, applyDisplacement to d)
                    else Exploration (to : walk) maybeSquare obstacle
            )
            ( \exploredPiece ->
                if Piece.color exploredPiece /= Piece.color piece
                  then Exploration [to] (Just . Square to $ Just exploredPiece) (Enemy, to)
                  else Exploration [] maybeLastVisitedSquare (Friendly, to)
            )
            $ pieceAt b to
        else Exploration [] maybeLastVisitedSquare (NoSpace, to)
      where
        to = applyDisplacement p d

data Attack
  = -- | Direct check.
    Direct Exploration
  | -- | An indirect attack, discovered check, becomes a real check by moving piece on pos out of the walk.
    Indirect Exploration Pos

-- | Positions through which `c` colored player receives attack or possibly discovered attack from.
attackWalks ::
  -- | Given a board.
  Board ->
  -- | Color of attacked player!
  Color ->
  -- | Position of square that we test for being attacked.
  Pos ->
  -- | Wheter to ignore first friendly piece. (Useful for searching for discovered checks.)
  Bool ->
  -- | Returns `Walk`s through which `Pos` is attacked.
  [Attack]
attackWalks b c p isAddDiscoveredAttacks =
  pawnAttacks ++ knightAttacks ++ queenAttackWalks
  where
    pawnAttacks =
      map (\p -> Direct $ Exploration [p] (Just . Square p . Just $ Piece (next c) Pawn) (Enemy, p))
        . filter (\p -> pieceAt b p == Just (Piece (next c) Pawn))
        $ map (\df -> Pos (rank p + colorMultiplier c) (file p + df)) [-1, 1]
    knightAttacks =
      map Direct
        . filter
          ( \(Exploration _ maybeSquare _) ->
              maybe
                False
                (\square -> piece square == Just (Piece (next c) Knight))
                maybeSquare
          )
        $ explore b p (Piece c Knight) One
    queenAttackWalks =
      catMaybes
        [ attack
          | d <- moveDirections Queen,
            let firstExploration@(Exploration walk maybeLastSquare (obstacle, p')) = exploreDirection b p (Piece c Queen) Inf d
                explorationAfterFriendly@(Exploration walk' maybeLastSquare' (obstacle', p'')) = exploreDirection b p' (Piece c Queen) Inf d
                bothExplorations = Exploration (walk ++ [p'] ++ walk') maybeLastSquare' (obstacle, p'')
                attack
                  | maybe False isAttacker maybeLastSquare = Just $ Direct firstExploration
                  | isAddDiscoveredAttacks && obstacle == Friendly && maybe False isAttacker maybeLastSquare' = Just $ Indirect bothExplorations p'
                  | otherwise = Nothing
        ]
      where
        isNextToKing, isLineAttacker, isDiagonallyAttacker :: Pos -> Piece -> Bool
        isNextToKing p' piece =
          (abs (rank p - rank p') == 1 || abs (rank p - rank p') == 0)
            && (abs (file p - file p') == 1 || abs (file p - file p') == 0)
            && piece == Piece (next c) King
        isLineAttacker p' piece =
          (rank p == rank p' || file p == file p')
            && c /= Piece.color piece
            && lineAttacker (name piece)
        isDiagonallyAttacker p' piece =
          (rank p /= rank p' && file p /= file p')
            && c /= Piece.color piece
            && diagonalAttacker (name piece)
        isAttacker :: Square -> Bool
        isAttacker s =
          maybe
            False
            ( \piece ->
                isNextToKing (position s) piece
                  || isLineAttacker (position s) piece
                  || isDiagonallyAttacker (position s) piece
            )
            $ piece s

-- | Generates list of legal moves of a certain piece.
genMoves ::
  -- | Given a board.
  Board ->
  -- | Try to move piece on position.
  Pos ->
  -- | Last move.
  Maybe Move ->
  -- | Wheter king or queen side castling is allowed.
  Castleable ->
  ([Walk], Walk, Bool, Bool, Bool) ->
  -- | All the possible moves.
  [Move]
genMoves b from lastMove castleable (checkWalks, discoveredCheckWalk, isCheck, isDiscoveredCheckDanger, isKingMustMove) = case b `pieceAt` from of
  Nothing -> []
  Just pieceToMove@(Piece c t) ->
    if isKingMustMove && t /= King
      then []
      else case t of
        Pawn ->
          catMaybes (oneForward ++ [twoForward] ++ captures ++ enPassants)
          where
            oneForward :: [Maybe Move]
            oneForward =
              let to = Pos (rank from + colorMultiplier c) (file from)
               in if validPos to
                    && isNothing (pieceAt b to)
                    && (not isCheck || (to `elem` head checkWalks))
                    && (not isDiscoveredCheckDanger || to `elem` discoveredCheckWalk)
                    then
                      if rank from == unifyRank 6 c
                        then map (Just . PawnPromotion from to) [Knight, Bishop, Rook, Queen]
                        else [Just $ Move from to]
                    else [Nothing]
            twoForward :: Maybe Move
            twoForward =
              let to = Pos (rank from + 2 * colorMultiplier c) (file from)
               in if validPos to
                    && (rank from == unifyRank 1 c)
                    && isNothing (pieceAt b (Pos (rank from + colorMultiplier c) (file to)))
                    && isNothing (pieceAt b to)
                    && (not isCheck || (to `elem` head checkWalks))
                    && (not isDiscoveredCheckDanger || to `elem` discoveredCheckWalk)
                    then Just $ Move from to
                    else Nothing
            captures, enPassants :: [Maybe Move]
            captures =
              let toRank = rank from + colorMultiplier c
                  tos =
                    [ Pos toRank (file from - 1),
                      Pos toRank (file from + 1)
                    ]
               in concatMap
                    ( \to ->
                        if maybe False (\pieceOnP -> Piece.color pieceOnP /= c) (pieceAt b to) -- if enemy piece
                          && (not isCheck || (to `elem` head checkWalks))
                          && (not isDiscoveredCheckDanger || to `elem` discoveredCheckWalk)
                          then
                            if rank from == unifyRank 6 c
                              then map (Just . PawnPromotion from to) [Knight, Bishop, Rook, Queen]
                              else [Just $ Move from to]
                          else [Nothing]
                    )
                    tos
            enPassants =
              let toRank = rank from + colorMultiplier c
                  tos =
                    [ Pos toRank (file from - 1),
                      Pos toRank (file from + 1)
                    ]
               in map
                    ( \to ->
                        let lastMoveWasTwoForward = lastMove == Just (Move (Pos (unifyRank 1 (next c)) (file to)) (Pos (unifyRank 3 (next c)) (file to))) -- it means that the `to` position is free
                            neighbourIsEnemyPawn = maybe False (\piece -> piece == Piece (next c) Pawn) (pieceAt b (Pos (rank from) (file to)))
                            enPassantable = lastMoveWasTwoForward && neighbourIsEnemyPawn
                         in if rank from == unifyRank 4 c
                              && enPassantable
                              && (not isCheck || (to `elem` head checkWalks))
                              && (not isDiscoveredCheckDanger || to `elem` discoveredCheckWalk)
                              then Just $ Move from to
                              else Nothing
                    )
                    tos
        Knight ->
          [ Move from to
            | (Exploration walk s _) <- explore b from pieceToMove One,
              to <- walk,
              not isCheck || to `elem` head checkWalks,
              not isDiscoveredCheckDanger
          ]
        King -> regularMoves ++ [KingSideCastle | kingSideCastle] ++ [QueenSideCastle | queenSideCastle]
          where
            regularMoves =
              [ Move from to
                | (Exploration walk s _) <- explore b from pieceToMove One,
                  to <- walk,
                  not $ isAttacked (deleteSquare from b) c to
              ]
            kingSideCastle = kingSide castleable && not isCheck && all (\p -> not (isAttacked b c p) && isNothing (pieceAt b p)) [Pos (unifyRank 0 c) 5, Pos (unifyRank 0 c) 6]
            queenSideCastle = queenSide castleable && not isCheck && all (\p -> not (isAttacked b c p) && isNothing (pieceAt b p)) [Pos (unifyRank 0 c) 2, Pos (unifyRank 0 c) 3]
        _ ->
          [ Move from to
            | (Exploration walk s _) <- explore b from pieceToMove Inf,
              to <- walk,
              not isCheck || to `elem` head checkWalks,
              not isDiscoveredCheckDanger || to `elem` discoveredCheckWalk
          ]

isAttacked :: Board -> Color -> Pos -> Bool
isAttacked b c p = not . null $ attackWalks b c p False

-- | Commits a `Move`.
applyMove :: Board -> Move -> Color -> Board
applyMove b m c = case m of
  Move p1 p2 ->
    if pieceAt b p1 == Just (Piece c Pawn)
      && file p1 /= file p2
      && isNothing (pieceAt b p2)
      then deleteSquare (Pos (rank p1) (file p2)) $ movePos p1 p2 b -- En Passant
      else movePos p1 p2 b
  KingSideCastle ->
    let r = unifyRank 0 c
     in movePos (Pos r 7) (Pos r 5) $ movePos (Pos r 4) (Pos r 6) b
  QueenSideCastle ->
    let r = unifyRank 0 c
     in movePos (Pos r 0) (Pos r 3) $ movePos (Pos r 4) (Pos r 2) b
  PawnPromotion p1 p2 t -> updateBoard p2 (Just (Piece c t)) $ deleteSquare p1 b
