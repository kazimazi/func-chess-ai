{
  description = "template for haskell packages";

  # Provides abstraction to boiler-code when specifying multi-platform outputs.
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem
      (
        system:
        let
          pkgs = import nixpkgs {
            system = "x86_64-linux";
            config.allowUnfree = true;
          };
        in
        {
          devShell = pkgs.mkShell {
            buildInputs = [
              (
                pkgs.ghc.withPackages (hp: with hp; [
                  haskell-language-server
                  htrace # for debugging
                ])
              )
            ];
          };
        }
      );
}
