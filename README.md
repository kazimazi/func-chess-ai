# Functional Chess AI

## Requirements

* ghc
* ghci *
* haskell-language-server *
* htrace *

_\* development dependencies_

## Building and running

```sh
git clone https://gitlab.com/kazimazi/func-chess-ai.git
cd func-chess-ai
ghc main.hs
./main
```

Players can be set as command line arguments.\
Players: **human**, **random**\
By **default** white is **human** controlled and **black** is random-AI.\
Example:
```sh
./main random random # random-AI vs. random-AI
./main human human # human vs. human
```

## How to play

When it's turn of the human player, a list of valid moves are presented, you can commit to a move either by using the appropriat number,
or by specifying where you want to move from and where you want to move to.\

Example of moves in the initial position:
```
e2e4
e2 e4
g1f3
g1 f3
```

Castling to the short and long side:
```
0-0
0-0-0
```

Promoting pawn into queen:
```
b8Q
```

## Pulling out development environment with Flake supported Nix

```
nix develop
```
