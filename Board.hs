module Board
  ( Square (Square, position, piece),
    Board,
    squares,
    initialBoard,
    emptyBoard,
    putBoard,
    pieceAt,
    movePos,
    colorPos,
    deleteSquare,
    updateBoard,
  )
where

import Color (Color (..))
import Data.List (intercalate)
import Piece
  ( Piece (Piece, color),
    PieceColor,
    PieceType (Bishop, King, Knight, Pawn, Queen, Rook),
    prettyPiece,
    valuePiece,
  )
import Position (Pos (Pos, file, rank), toIndex)

data Square = Square
  { position :: Pos,
    piece :: Maybe Piece
  }
  deriving (Eq)

instance Show Square where
  show (Square p pc) = show p ++ " " ++ show pc

type Board = [Square]

-- | Generates all 64 squares of our plane.
squares :: [Pos]
squares = [Pos r f | r <- [0 .. 7], f <- [0 .. 7]]

-- | Fills the board with chess pieces to their initial positions.
initialBoard :: Board
initialBoard =
  let hindRank = [Rook, Knight, Bishop, Queen, King, Bishop, Knight, Rook]
      pawnRank = replicate 8 Pawn
      whiteHindRank = map (Just . Piece White) hindRank
      whitePawnRank = map (Just . Piece White) pawnRank
      blackHindRank = map (Just . Piece Black) hindRank
      blackPawnRank = map (Just . Piece Black) pawnRank
      emptyRanks = replicate 32 Nothing
      pieces = whiteHindRank ++ whitePawnRank ++ emptyRanks ++ blackPawnRank ++ blackHindRank
      positions = squares
   in zipWith Square positions pieces

-- | Completely empty chess board.
emptyBoard :: Board
emptyBoard = map (`Square` Nothing) squares

prettyBoard :: Board -> String
prettyBoard board = prettyBoardHelper board 1
  where
    height = 3 -- FIXME fine-tuning required here
    width = 4 -- Only evens look nice.
    prettySquare :: Square -> String
    prettySquare (Square pos piece) =
      if file pos == 7 then box ++ rankCloser else box
      where
        -- █░
        squareColor = if odd (rank pos + file pos) then '░' else ' '
        box =
          maybe
            (squareColor : replicate width squareColor ++ [squareColor])
            ( \a ->
                squareColor :
                replicate (width `div` 2 - 1) squareColor
                  ++ prettyPiece a
                  ++ [' ']
                  ++ replicate (width `div` 2) squareColor
            )
            piece
        rankCloser = "\n"
    prettyBoardHelper :: Board -> Int -> String
    prettyBoardHelper board i
      | null board = ""
      | length board == 64 = ranks ++ fileIndicators
      | otherwise = ranks
      where
        flats = replicate width '─'
        fileIndicators = replicate width ' ' ++ intercalate (replicate (width + 1) ' ') [['a'], ['b'], ['c'], ['d'], ['e'], ['f'], ['g'], ['h']] ++ "\n"
        (headBoard, tailBoard) = splitAt 8 board
        emptySquares = map (\s -> Square (position s) Nothing) headBoard
        ranks = nextRank ++ emptyRanks ++ middleOfARank ++ emptyRanks
          where
            nextRank = prettyBoardHelper tailBoard (i + 1)
            emptyRanks = concat (replicate (height `div` 2) (" " ++ concatMap prettySquare emptySquares))
            middleOfARank = show i ++ concatMap prettySquare headBoard

putBoard :: Board -> IO ()
putBoard = putStr . prettyBoard

evalBoard :: Board -> Int
evalBoard [] = 0
evalBoard (s : b) =
  let rest = evalBoard b
   in maybe rest (\piece -> valuePiece piece + rest) (piece s)

updateBoard :: Pos -> Maybe Piece -> Board -> Board
updateBoard _ _ [] = []
updateBoard p pc (s : b) =
  if position s == p
    then Square p pc : b
    else s : updateBoard p pc b

deleteSquare :: Pos -> Board -> Board
deleteSquare _ [] = []
deleteSquare p (s : b) =
  if p == position s
    then Square p Nothing : b
    else s : deleteSquare p b

pieceAt :: Board -> Pos -> Maybe Piece
pieceAt [] _ = Nothing
pieceAt (s : b) p = if p == position s then piece s else pieceAt b p

-- | A more efficient way of moving a piece across the board, with only one traversal.
movePos :: Pos -> Pos -> Board -> Board
movePos p1 p2 = moveIndex (toIndex p1) (toIndex p2)

moveIndex :: Int -> Int -> Board -> Board
moveIndex _ _ [] = []
moveIndex 0 j b =
  let (jth, ith : b') = moveIndexHelper j b (piece ith)
   in Square (position ith) Nothing : b'
moveIndex i 0 b =
  let (jth, ith : b') = moveIndexHelper i b Nothing
   in Square (position ith) jth : b'
moveIndex i j (s : b) = s : moveIndex (i - 1) (j - 1) b

-- | Pops nth Piece, replaces it with x and returns the popped Piece and new Board.
moveIndexHelper :: Int -> Board -> Maybe Piece -> (Maybe Piece, Board)
moveIndexHelper _ [] x = (x, [])
moveIndexHelper 0 (h : t) x = (piece h, Square (position h) x : t)
moveIndexHelper n (h : t) x = (y, h : t')
  where
    (y, t') = moveIndexHelper (n - 1) t x

colorPos :: PieceColor -> Board -> [Pos]
colorPos _ [] = []
colorPos c (s : b) =
  let rest = colorPos c b
   in maybe rest (\piece -> if c == color piece then position s : rest else rest) (piece s)
